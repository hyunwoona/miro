// arr must be an array of objects that has 'id' key.
function arrayToObjWithIdKey(arr) {
  return arr.reduce((acc, person) => {
    acc[person.id] = person;
    return acc;
  }, {});
}

function getIdFromGhibliUrl(url) {
  return url && url.substr(url.lastIndexOf('/') + 1);
}

export { arrayToObjWithIdKey, getIdFromGhibliUrl };
