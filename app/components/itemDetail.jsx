import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import ButtonBase from 'material-ui/ButtonBase';
import FavoriteIcon from 'material-ui-icons/Favorite';
import FavoriteBorderIcon from 'material-ui-icons/FavoriteBorder';

import { getIdFromGhibliUrl } from '../utils';

const FavoriteButton = ({ isFavorite, toggleFavorite }) => (
  <ButtonBase
    style={{ right: 20, position: 'absolute' }}
    onClick={toggleFavorite}
  >
    {isFavorite ? <FavoriteIcon style={{ color: 'red' }} /> : <FavoriteBorderIcon />}
  </ButtonBase>
);

class ItemDetail extends React.Component {
  render() {
    const { item, type, people, species, films, toggleFavorite } = this.props;

    if (type === 'people') {
      const speciesId = getIdFromGhibliUrl(item.species);
      const speciesDetailPath = `/species/${speciesId}`;
      const speciesName = speciesId && speciesId !== ' ' && species[speciesId].name;

      const filmIds = item.films.map(film => getIdFromGhibliUrl(film)).filter(String);
      const filmography = filmIds.map((filmId) => {
        const filmDetailPath = `/films/${filmId}`;
        const filmTitle = films[filmId].title;
        return (<p key={filmId}><Link to={filmDetailPath}>{filmTitle}</Link></p>);
      });
      return (
        <div>
          <FavoriteButton
            isFavorite={item.isFavorite}
            toggleFavorite={() => toggleFavorite(item.id, type)}
          />
          <h3>Detail about this person</h3>
          <p>Name: {item.name}</p>
          <p>Gender: {item.gender}</p>
          <p>Age: {item.age}</p>
          <p>Eye color: {item.eye_color}</p>
          <p>Hair color: {item.hair_color}</p>
          {filmography.length > 0 && <div><span>Films:</span>{filmography}</div>}
          <p>Species: <Link to={speciesDetailPath}>{speciesName}</Link></p>
        </div>
      );
    } else if (type === 'films') {
      const peopleIds = item.people.map(person => getIdFromGhibliUrl(person)).filter(String);
      const peopleInTheFilm = peopleIds.map((personId) => {
        const personDetailPath = `/people/${personId}`;
        const personName = people[personId].name;
        return (<p key={personId}><Link to={personDetailPath}>{personName}</Link></p>);
      });
      const speciesIds = item.species.map(person => getIdFromGhibliUrl(person)).filter(String);
      const speciesInTheFilm = speciesIds.map((speciesId) => {
        const speciesDetailPath = `/species/${speciesId}`;
        const speciesName = species[speciesId].name;
        return (<p key={speciesId}><Link to={speciesDetailPath}>{speciesName}</Link></p>);
      });
      return (
        <div>
          <FavoriteButton
            isFavorite={item.isFavorite}
            toggleFavorite={() => toggleFavorite(item.id, type)}
          />
          <h3>Detail about this film</h3>
          <p>Title: {item.title}</p>
          <p>Description: {item.description}</p>
          <p>Director: {item.director}</p>
          <p>Producer: {item.producer}</p>
          <p>Rotten tomato score: {item.rt_score}</p>
          <p>Release date: {item.release_date}</p>
          {peopleInTheFilm.length > 0 && <div><span>People:</span>{peopleInTheFilm}</div>}
          {speciesInTheFilm.length > 0 && <div><span>Species:</span>{speciesInTheFilm}</div>}
        </div>
      );
    } else if (type === 'species') {
      const peopleIds = item.people.map(person => getIdFromGhibliUrl(person)).filter(String);
      const peopleInTheSpecies = peopleIds.map((personId) => {
        const personDetailPath = `/people/${personId}`;
        const personName = people[personId].name;
        return (<p key={personId}><Link to={personDetailPath}>{personName}</Link></p>);
      });
      const filmIds = item.films.map(film => getIdFromGhibliUrl(film)).filter(String);
      const filmsWithThisSpecies = filmIds.map((filmId) => {
        const filmDetailPath = `/films/${filmId}`;
        const filmTitle = films[filmId].title;
        return (<p key={filmId}><Link to={filmDetailPath}>{filmTitle}</Link></p>);
      });

      return (
        <div>
          <FavoriteButton
            isFavorite={item.isFavorite}
            toggleFavorite={() => toggleFavorite(item.id, type)}
          />
          <h3>Detail about this species</h3>
          <p>Name: {item.name}</p>
          <p>Classification: {item.classification}</p>
          <p>Eye colors: {item.eye_colors}</p>
          <p>Hair colors: {item.hair_colors}</p>
          {peopleInTheSpecies.length > 0 && <div><span>People:</span>{peopleInTheSpecies}</div>}
          {filmsWithThisSpecies.length > 0 && <div><span>Films:</span>{filmsWithThisSpecies}</div>}
        </div>
      );
    }
    return null;
  }
}

ItemDetail.propTypes = {
  item: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
  people: PropTypes.object.isRequired,
  films: PropTypes.object.isRequired,
  species: PropTypes.object.isRequired,
  toggleFavorite: PropTypes.func,
};

FavoriteButton.propTypes = {
  isFavorite: PropTypes.bool.isRequired,
  toggleFavorite: PropTypes.func,
};

export default ItemDetail;
