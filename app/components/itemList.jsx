import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

const ItemLink = ({ item, type }) => (
  <NavLink
    to={`/${type}/${item.id}`}
    activeStyle={{ fontWeight: 500 }}
  >
    <div className="link-info">
      { type === 'people' &&
        <p>
          Name: {item.name}, Gender: {item.gender}, Age: {item.age}
        </p>
      }
      { type === 'films' &&
        <p>
          Name: {item.title}, Director: {item.director}, Released: {item.release_date}
        </p>
      }
      { type === 'species' &&
        <p>
          Name: {item.name}, Classification: {item.classification}
        </p>
      }
    </div>
  </NavLink>
);

class ItemList extends React.Component {
  containsId(id, item) {
    if (Array.isArray(item)) {
      return item.some(arrayElem => this.containsId(id, arrayElem));
    } else if (typeof item === 'object') {
      return this.containsId(id, Object.values(item));
    } else if (typeof item === 'string') {
      return item.includes(id);
    }
    return false;
  }

  filterBySearchSelectedId(id, items) {
    return items.filter(item => this.containsId(id, Object.values(item)));
  }

  render() {
    const { items, showFavoritesOnly, filterBySelected, selectedFromSearch } = this.props;
    let itemsArray = items;
    if (showFavoritesOnly) {
      itemsArray = itemsArray.filter(item => item.isFavorite);
    }
    if (filterBySelected) {
      const selectedId = selectedFromSearch.id;
      itemsArray = this.filterBySearchSelectedId(selectedId, itemsArray);
    }
    return (
      <ul style={{
        listStyle: 'none',
        padding: 0 }}
      >
        { itemsArray.map((item) => {
          return (<li key={item.id}>
            <ItemLink item={item} type={item.type} />
          </li>);
        }) }
      </ul>
    );
  }
}

ItemLink.propTypes = {
  item: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
};

ItemList.propTypes = {
  items: PropTypes.array.isRequired,
  showFavoritesOnly: PropTypes.bool.isRequired,
  filterBySelected: PropTypes.bool,
  selectedFromSearch: PropTypes.object,
};

export default ItemList;
