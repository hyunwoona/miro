import * as actionType from '../constants/actionTypes/menu';

const initialState = {
  people: {}, // maps id to person object
  peopleReceiveInProgress: false,
  peopleReceiveFailure: false,
  films: {}, // maps id to film object
  filmsReceiveInProgress: false,
  filmsReceiveFailure: false,
  species: {}, // maps id to species object
  speciesReceiveInProgress: false,
  speciesReceiveFailure: false,
};

export default function menu(state = initialState, action) {
  switch (action.type) {
    case actionType.REQUEST_PEOPLE:
      return {
        ...state,
        peopleReceiveInProgress: true,
      };
    case actionType.REQUEST_PEOPLE_FAILURE:
      return {
        ...state,
        peopleReceiveInProgress: false,
        peopleReceiveFailure: true,
      };
    case actionType.RECEIVE_PEOPLE:
      return {
        ...state,
        people: action.people,
        peopleReceiveInProgress: false,
        peopleReceiveFailure: false,
      };
    case actionType.REQUEST_FILMS:
      return {
        ...state,
        filmsReceiveInProgress: true,
      };
    case actionType.REQUEST_FILMS_FAILURE:
      return {
        ...state,
        filmsReceiveInProgress: false,
        filmsReceiveFailure: true,
      };
    case actionType.RECEIVE_FILMS:
      return {
        ...state,
        films: action.films,
        filmsReceiveInProgress: false,
        filmsReceiveFailure: false,
      };
    case actionType.REQUEST_SPECIES:
      return {
        ...state,
        speciesReceiveInProgress: true,
      };
    case actionType.REQUEST_SPECIES_FAILURE:
      return {
        ...state,
        speciesReceiveInProgress: false,
        speciesReceiveFailure: true,
      };
    case actionType.RECEIVE_SPECIES:
      return {
        ...state,
        species: action.species,
        speciesReceiveInProgress: false,
        speciesReceiveFailure: false,
      };
    case actionType.TOGGLE_FAVORITE_PERSON: {
      const people = state.people;
      const favoriteToggledPeople = {
        ...people,
        [action.id]: {
          ...people[action.id],
          isFavorite: !(people[action.id].isFavorite),
        },
      };
      return {
        ...state,
        people: favoriteToggledPeople,
      };
    }
    case actionType.TOGGLE_FAVORITE_FILM: {
      const films = state.films;
      const favoriteToggledFilms = {
        ...films,
        [action.id]: {
          ...films[action.id],
          isFavorite: !(films[action.id].isFavorite),
        },
      };
      return {
        ...state,
        films: favoriteToggledFilms,
      };
    }
    case actionType.TOGGLE_FAVORITE_SPECIES: {
      const species = state.species;
      const favoriteToggledSpecies = {
        ...species,
        [action.id]: {
          ...species[action.id],
          isFavorite: !(species[action.id].isFavorite),
        },
      };
      return {
        ...state,
        species: favoriteToggledSpecies,
      };
    }
    default:
      return state;
  }
}
