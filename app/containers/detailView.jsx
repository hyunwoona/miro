import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, withRouter } from 'react-router-dom';
import ItemDetail from '../components/itemDetail';
import { toggleFavoritePerson, toggleFavoriteFilm, toggleFavoriteSpecies } from '../actions/menu';

class DetailView extends React.Component {
  constructor(props) {
    super(props);
    this.handleFavoriteClick = this.handleFavoriteClick.bind(this);
  }

  handleFavoriteClick(id, type) {
    const { dispatch } = this.props;

    if (type === 'people') {
      dispatch(toggleFavoritePerson(id));
    } else if (type === 'films') {
      dispatch(toggleFavoriteFilm(id));
    } else if (type === 'species') {
      dispatch(toggleFavoriteSpecies(id));
    }
  }

  render() {
    const { dispatch } = this.props;
    const { people, peopleReceiveInProgress, peopleReceiveFailure,
      films, filmsReceiveInProgress, filmsReceiveFailure,
      species, speciesReceiveInProgress, speciesReceiveFailure } = this.props;

    if (peopleReceiveFailure || filmsReceiveFailure || speciesReceiveFailure) {
      return (<p>Failure receiving the list</p>);
    }

    if (peopleReceiveInProgress || filmsReceiveInProgress || speciesReceiveInProgress) {
      return (<p>Loading...</p>);
    }

    return (
      <div
        className="detail-view"
        style={{
          border: '1px solid',
          padding: '40px 10px',
          height: 500,
          overflowY: 'scroll',
        }}
      >
        <Route
          path="/people/:id"
          render={({ match }) => {
            const person = people[match.params.id];
            return (person ?
              <ItemDetail
                item={person}
                type="people"
                people={people}
                films={films}
                species={species}
                toggleFavorite={this.handleFavoriteClick}
              /> : null
            );
          }}
        />
        <Route
          path="/films/:id"
          render={({ match }) => {
            const film = films[match.params.id];
            return (film ?
              <ItemDetail
                item={film}
                type="films"
                people={people}
                films={films}
                species={species}
                toggleFavorite={this.handleFavoriteClick}
              /> : null
            );
          }}
        />
        <Route
          path="/species/:id"
          render={({ match }) => {
            const speciesItem = species[match.params.id];
            return (speciesItem ?
              <ItemDetail
                item={speciesItem}
                type="species"
                people={people}
                films={films}
                species={species}
                toggleFavorite={this.handleFavoriteClick}
              /> : null
            );
          }}
        />
      </div>
    );
  }
}

DetailView.propTypes = {
  dispatch: PropTypes.func.isRequired,
  people: PropTypes.object.isRequired,
  peopleReceiveInProgress: PropTypes.bool,
  peopleReceiveFailure: PropTypes.bool,
  films: PropTypes.object.isRequired,
  filmsReceiveInProgress: PropTypes.bool,
  filmsReceiveFailure: PropTypes.bool,
  species: PropTypes.object.isRequired,
  speciesReceiveInProgress: PropTypes.bool,
  speciesReceiveFailure: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    ...state.menu,
  };
}

export default withRouter(connect(mapStateToProps)(DetailView));
