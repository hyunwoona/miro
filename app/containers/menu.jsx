import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink, Route, withRouter } from 'react-router-dom';
import Switch from 'material-ui/Switch';
import { FormControlLabel } from 'material-ui/Form';

import { fetchPeople, fetchFilms, fetchSpecies } from '../actions/menu';
import IntegrationAutosuggest from '../components/integrationAutosuggest';
import ItemList from '../components/itemList';

function getSuggestions(value, suggestions) {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;
  let count = 0;

  return inputLength === 0
    ? []
    : suggestions.filter((suggestion) => {
      const keep =
        count < 5 && suggestion.name.toLowerCase().slice(0, inputLength) === inputValue;

      if (keep) {
        count += 1;
      }

      return keep;
    });
}

class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showFavoritesOnly: false,
      searchString: '',
      searchSuggestions: [],
      selectedFromSearch: null,
    };
    this.toggleShowFavoritesOnly = this.toggleShowFavoritesOnly.bind(this);
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchPeople());
    dispatch(fetchFilms());
    dispatch(fetchSpecies());
  }

  toggleShowFavoritesOnly() {
    this.setState(prevState => ({ showFavoritesOnly: !prevState.showFavoritesOnly }));
  }

  render() {
    const { people, peopleReceiveInProgress, peopleReceiveFailure,
      films, filmsReceiveInProgress, filmsReceiveFailure,
      species, speciesReceiveInProgress, speciesReceiveFailure } = this.props;
    const { selectedFromSearch } = this.state;

    if (peopleReceiveFailure || filmsReceiveFailure || speciesReceiveFailure) {
      return (<p>Failure receiving the list</p>);
    }

    if (peopleReceiveInProgress || filmsReceiveInProgress || speciesReceiveInProgress) {
      return (<p>Loading...</p>);
    }

    const searchSuggestions = [
      ...Object.values(people).map(person => ({ type: 'people', id: person.id, name: person.name })),
      ...Object.values(films).map(film => ({ type: 'films', id: film.id, name: film.title })),
    ];

    return (
      <div
        className="list"
        style={{
          border: '1px solid',
          padding: '40px 10px',
          height: 500,
          overflowY: 'scroll',
        }}
      >
        <h2>Ghibli Studio</h2>
        <IntegrationAutosuggest
          suggestions={this.state.searchSuggestions}
          searchString={this.state.searchString}
          handleSearchStringChange={(e, { newValue }) => this.setState({
            searchString: newValue,
          })}
          handleSuggestionsFetchRequested={(value) => {
            this.setState({
              searchSuggestions: getSuggestions(value, searchSuggestions),
            });
          }}
          handleSuggestionsClearRequested={() => {
            this.setState({
              searchSuggestions: [],
            });
            if (this.state.searchString === '') {
              this.setState({
                selectedFromSearch: null,
              })
            }
          }}
          onSuggestionSelected={(event, { suggestion }) => {
            this.setState({
              selectedFromSearch: suggestion,
            })
          }}
        />

        {!selectedFromSearch &&
          <div>
            <div>
              Categories:
              <NavLink to="/people" style={{ margin: '0 5px' }} activeStyle={{ color: 'red', fontWeight: 500 }}>
                People
              </NavLink>
              <NavLink to="/films" style={{ margin: '0 5px' }} activeStyle={{ color: 'red', fontWeight: 500 }}>
                Films
              </NavLink>
              <NavLink to="/species" style={{ margin: '0 5px' }} activeStyle={{ color: 'red', fontWeight: 500 }}>
                Species
              </NavLink>
            </div>
            <FormControlLabel
              control={
                <Switch
                  checked={this.state.showFavoritesOnly}
                  onChange={this.toggleShowFavoritesOnly}
                  aria-label="checkedShowFavoritesOnly"
                />
              }
              label="Show favorites only"
            />
            <hr />
          </div>
        }
        {
          selectedFromSearch &&
          <div>
            <p>Search results: items that contain '{selectedFromSearch.name}'</p>
            <p>(To go back, clear the search form and click outside)</p>
            <ItemList
              items={[
                ...Object.values(people),
                ...Object.values(films),
                ...Object.values(species),
              ]}
              showFavoritesOnly={false}
              filterBySelected
              selectedFromSearch={selectedFromSearch}
            />
          </div>
        }
        {
          !selectedFromSearch &&
          <Route
            path="/people"
            render={() => (
              peopleReceiveInProgress ?
                (<span> loading people... </span>) :
                (<ItemList
                  items={Object.values(people)}
                  showFavoritesOnly={this.state.showFavoritesOnly}
                />)
            )}
          />
        }
        {
          !selectedFromSearch &&
          <Route
            path="/films"
            render={() => (
              filmsReceiveInProgress ?
                (<span> loading films... </span>) :
                (<ItemList
                  items={Object.values(films)}
                  showFavoritesOnly={this.state.showFavoritesOnly}
                />)
            )}
          />
        }
        {
          !selectedFromSearch &&
          <Route
            path="/species"
            render={() => (
              speciesReceiveInProgress ?
                (<span> loading species... </span>) :
                (<ItemList
                  items={Object.values(species)}
                  showFavoritesOnly={this.state.showFavoritesOnly}
                />)
            )}
          />
        }
      </div>
    );
  }
}

Menu.propTypes = {
  dispatch: PropTypes.func.isRequired,
  people: PropTypes.object.isRequired,
  peopleReceiveInProgress: PropTypes.bool,
  peopleReceiveFailure: PropTypes.bool,
  films: PropTypes.object.isRequired,
  filmsReceiveInProgress: PropTypes.bool,
  filmsReceiveFailure: PropTypes.bool,
  species: PropTypes.object.isRequired,
  speciesReceiveInProgress: PropTypes.bool,
  speciesReceiveFailure: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    ...state.menu,
  };
}

export default withRouter(connect(mapStateToProps)(Menu));
