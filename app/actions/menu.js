import 'whatwg-fetch';
import { arrayToObjWithIdKey } from '../utils';
import * as actionType from '../constants/actionTypes/menu';

function requestPeople() {
  return {
    type: actionType.REQUEST_PEOPLE,
  };
}

function requestPeopleFailure() {
  return {
    type: actionType.REQUEST_PEOPLE_FAILURE,
  };
}

function receivePeople(peopleArray) {
  const peopleArrayWithFavorite = peopleArray.map(person => (
    {
      ...person,
      type: 'people',
      isFavorite: false,
    }
  ));

  const idToPersonMapping = arrayToObjWithIdKey(peopleArrayWithFavorite);
  return {
    type: actionType.RECEIVE_PEOPLE,
    people: idToPersonMapping,
  };
}

function requestFilms() {
  return {
    type: actionType.REQUEST_FILMS,
  };
}

function requestFilmsFailure() {
  return {
    type: actionType.REQUEST_FILMS_FAILURE,
  };
}

function receiveFilms(filmsArray) {
  const filmsArrayWithFavorite = filmsArray.map(film => (
    {
      ...film,
      type: 'films',
      isFavorite: false,
    }
  ));
  const idToFilmMapping = arrayToObjWithIdKey(filmsArrayWithFavorite);
  return {
    type: actionType.RECEIVE_FILMS,
    films: idToFilmMapping,
  };
}

function requestSpecies() {
  return {
    type: actionType.REQUEST_SPECIES,
  };
}

function requestSpeciesFailure() {
  return {
    type: actionType.REQUEST_SPECIES_FAILURE,
  };
}

function receiveSpecies(speciesArray) {
  const speciesArrayWithFavorite = speciesArray.map(speciesItem => (
    {
      ...speciesItem,
      type: 'species',
      isFavorite: false,
    }
  ));
  const idToLocationMapping = arrayToObjWithIdKey(speciesArrayWithFavorite);
  return {
    type: actionType.RECEIVE_SPECIES,
    species: idToLocationMapping,
  };
}

export function fetchPeople() {
  return (dispatch) => {
    dispatch(requestPeople());
    const url = 'https://ghibliapi.herokuapp.com/people';
    return fetch(url).then((response) => {
      return response.json();
    }).then((data) => {
      dispatch(receivePeople(data));
    }).catch(() => {
      dispatch(requestPeopleFailure());
    });
  }
}

export function fetchFilms() {
  return (dispatch) => {
    dispatch(requestFilms());
    const url = 'https://ghibliapi.herokuapp.com/films';
    return fetch(url).then((response) => {
      return response.json();
    }).then((data) => {
      dispatch(receiveFilms(data));
    }).catch(() => {
      dispatch(requestFilmsFailure());
    });
  }
}

export function fetchSpecies() {
  return (dispatch) => {
    dispatch(requestSpecies());
    const url = 'https://ghibliapi.herokuapp.com/species';
    return fetch(url).then((response) => {
      return response.json();
    }).then((data) => {
      dispatch(receiveSpecies(data));
    }).catch(() => {
      dispatch(requestSpeciesFailure());
    });
  }
}

export function toggleFavoritePerson(id) {
  return {
    type: actionType.TOGGLE_FAVORITE_PERSON,
    id,
  };
}

export function toggleFavoriteFilm(id) {
  return {
    type: actionType.TOGGLE_FAVORITE_FILM,
    id,
  };
}

export function toggleFavoriteSpecies(id) {
  return {
    type: actionType.TOGGLE_FAVORITE_SPECIES,
    id,
  };
}
